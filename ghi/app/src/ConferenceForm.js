import React from 'react';

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      description: '',
      ends: '',
      max_attendees: '',
      max_presentations: '',
      name: '',
      starts: '',
      locations: [],
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
    this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  handleStartDateChange(event) {
    const value = event.target.value;
    this.setState({ starts: value });
  }

  handleEndDateChange(event) {
    const value = event.target.value;
    this.setState({ ends: value });
  }

  handleDescriptionChange(event) {
    const value = event.target.value;
    this.setState({ description: value });
  }

  handleMaxPresentationChange(event) {
    const value = event.target.value;
    this.setState({ max_presentations: value });
  }

  handleMaxAttendeesChange(event) {
    const value = event.target.value;
    this.setState({ max_attendees: value });
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.locations;

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {

      const cleared = {
        description: '',
        ends: '',
        location: '',
        max_attendees: '',
        max_presentations: '',
        name: '',
        starts: '',
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations })
    }

  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartDateChange} value={this.state.start_date} required type="date" name="starts" id="start_date" className="form-control" />
                <label htmlFor="start_date">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndDateChange} value={this.state.end_date} required type="date" name="ends" id="end_date" className="form-control" />
                <label htmlFor="end_date">End date</label>
              </div>
              <div>
                <input onChange={this.handleDescriptionChange} value={this.state.description} required type="textarea" name="description" id="description" className="form-control" />
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentationChange} value={this.state.max_presentations} placeholder="Max presentations" required type="number" name="max_presentations"
                  id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeesChange} value={this.state.max_attendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees"
                  className="form-control" />
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.href} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;