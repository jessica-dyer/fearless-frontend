import React from 'react'

export default function CurrencyRow(props) {
  const {
    currencyOptions,
    selectedCurrency,
    onChangeCurrency,
    onChangeAmount,
    amount,
  } = props

  return (
    <div className="d-flex justify-content-center">
      <input className="form-control form-rounded w-25" type="number" value={amount} onChange={onChangeAmount} />
      <select value={selectedCurrency} onChange={onChangeCurrency}>
        {currencyOptions.map(option =>
          <option value={option} key={option}>{option}</option>)}
        {/* <option value="hi">Hi</option> */}
      </select>
    </div>
  )
}