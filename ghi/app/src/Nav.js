import { NavLink, Outlet } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to='' style={({ isActive }) => {
          return {
            color: isActive ? "red" : "",
          };
        }}>Conference GO!</NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="" style={({ isActive }) => {
                return {
                  color: isActive ? "red" : "",
                };
              }} >Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new_location_nav_link" aria-current="page" style={({ isActive }) => {
                return {
                  color: isActive ? "red" : "",
                };
              }} to="/locations/new">New
                location</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new_conference_nav_link" aria-current="page" style={({ isActive }) => {
                return {
                  color: isActive ? "red" : "",
                };
              }} to="/conferences/new">New
                conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new_conference_nav_link" aria-current="page" style={({ isActive }) => {
                return {
                  color: isActive ? "red" : "",
                };
              }} to="/presentations/new">New
                presentation</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="test_page" aria-current="page" style={({ isActive }) => {
                return {
                  color: isActive ? "red" : "",
                };
              }} to="/test">Test page</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="converter" aria-current="page" style={({ isActive }) => {
                return {
                  color: isActive ? "red" : "",
                };
              }} to="/converter">Currency Converter</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;